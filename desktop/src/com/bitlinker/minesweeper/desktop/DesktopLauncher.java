package com.bitlinker.minesweeper.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.tools.texturepacker.TexturePacker;
import com.bitlinker.minesweeper.Constants;
import com.bitlinker.minesweeper.Minesweeper;

import java.io.File;

public class DesktopLauncher {
	private static boolean mRebuildAtlas = false;
	private static boolean mDrawDebugOutline = false;

	public static void main (String[] arg) {

	    // DBG: rebuild atlas if deleted
	    File fileAtlas = new File("./images/minesweeper.pack.atlas");
        mRebuildAtlas = !fileAtlas.exists();

		if (mRebuildAtlas) {
			TexturePacker.Settings settings = new TexturePacker.Settings();
			settings.maxWidth = 2048;
			settings.maxHeight = 2048;
			settings.duplicatePadding = false;
			settings.debug = mDrawDebugOutline;
			TexturePacker.process(settings, "../assets-raw/images", "images", "minesweeper.pack");
			TexturePacker.process(settings, "../assets-raw/images-ui", "images", "minesweeper-ui.pack");
		}

		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = (int)Constants.VIEWPORT_GUI_WIDTH;
		config.height = (int)Constants.VIEWPORT_GUI_HEIGHT;
		new LwjglApplication(new Minesweeper(), config);
	}
}
