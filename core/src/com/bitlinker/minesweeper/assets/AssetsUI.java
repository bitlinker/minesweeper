package com.bitlinker.minesweeper.assets;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import org.jetbrains.annotations.NotNull;

public class AssetsUI {
    private final TextureRegion regionSplashBg;
    private final TextureRegion regionSplashTitle;

    public AssetsUI(@NotNull TextureAtlas uiAtlas) {
        regionSplashBg = uiAtlas.findRegion("logo");
        regionSplashTitle = uiAtlas.findRegion("title");
    }

    // TODO: no getters?
    public @NotNull
    TextureRegion getSplashBgRegion() {
        return regionSplashBg;
    }


    public @NotNull TextureRegion getSplashTitleRegion() {
        return regionSplashTitle;
    }
}
