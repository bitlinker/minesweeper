package com.bitlinker.minesweeper.assets;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class AssetsGame {
    public final TextureRegion regionCellClosed;
    public final TextureRegion regionCellClosedPressed;
    public final TextureRegion[] regionCellOpened;
    public final TextureRegion regionCellFlag;
    public final TextureRegion regionCellQuestion;
    public final TextureRegion regionMine;

    public final TextureRegion borderTL;
    public final TextureRegion borderTC;
    public final TextureRegion borderTR;
//    public final TextureRegion borderCL;
//    public final TextureRegion borderCC;
//    public final TextureRegion borderCR;
//    public final TextureRegion borderBL;
//    public final TextureRegion borderBC;
//    public final TextureRegion borderBR;

    // TODO: getters?

    public AssetsGame(@NotNull TextureAtlas gameAtlas) throws IOException {
        regionCellClosed = findRegionAsserted(gameAtlas, "classic/cell_closed", -1);
        regionCellClosedPressed = findRegionAsserted(gameAtlas, "classic/cell_press", -1);
        regionCellFlag = findRegionAsserted(gameAtlas, "classic/flag", -1);
        regionCellQuestion = findRegionAsserted(gameAtlas, "classic/mark", -1);
        regionMine = findRegionAsserted(gameAtlas, "classic/mine", -1);

        regionCellOpened = new TextureRegion[9];
        for (int i = 0; i < regionCellOpened.length; ++i) {
            regionCellOpened[i] = findRegionAsserted(gameAtlas, "classic/cell", i);
        }

        // TODO: move to ui?
        borderTL = findRegionAsserted(gameAtlas, "classic/border_tl", -1);
        borderTC = findRegionAsserted(gameAtlas, "classic/border_tc", -1);
        borderTR = findRegionAsserted(gameAtlas, "classic/border_tr", -1);
//        borderCL = findRegionAsserted(gameAtlas, "classic/border_cl", -1);
//        borderCC = findRegionAsserted(gameAtlas, "classic/border_cc", -1);
//        borderCR = findRegionAsserted(gameAtlas, "classic/border_cr", -1);
//        borderBL = findRegionAsserted(gameAtlas, "classic/border_bl", -1);
//        borderBC = findRegionAsserted(gameAtlas, "classic/border_bc", -1);
//        borderBR = findRegionAsserted(gameAtlas, "classic/border_br", -1);
    }

    private @NotNull
    TextureRegion findRegionAsserted(@NotNull TextureAtlas atlas, @NotNull String name, int index) throws IOException {
        TextureAtlas.AtlasRegion region;
        if (index != -1) {
            region = atlas.findRegion(name, index);
        } else {
            region = atlas.findRegion(name);
        }
        if (region == null) {
            throw new IOException("Can't find texture region: " + name);
        }
        return region;
    }
}
