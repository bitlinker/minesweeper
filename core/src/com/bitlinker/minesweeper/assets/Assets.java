package com.bitlinker.minesweeper.assets;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Disposable;
import com.bitlinker.hophop.Constants;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class Assets implements Disposable, AssetErrorListener {
    private static final String TAG = "Assets";

    private static final String ATLAS_GAME = "images/minesweeper.pack.atlas";
    private static final String ATLAS_UI = "images/minesweeper-ui.pack.atlas";

    private AssetManager assetManager;
    private TextureAtlas uiAtlas;
    private TextureAtlas gameAtlas;

    private AssetsGame assetsGame;
    private AssetsUI assetsUI;

    public void init() throws IOException {
        this.assetManager = new AssetManager();
        assetManager.setErrorListener(this);
        assetManager.load(ATLAS_GAME, TextureAtlas.class);
        assetManager.load(ATLAS_UI, TextureAtlas.class);
        assetManager.finishLoading();

        gameAtlas = assetManager.get(ATLAS_GAME);
        uiAtlas = assetManager.get(ATLAS_UI);
        setAtlasFilters(uiAtlas, Texture.TextureFilter.Linear);
        setAtlasFilters(gameAtlas, Texture.TextureFilter.Linear);

        assetsGame = new AssetsGame(gameAtlas);
        assetsUI = new AssetsUI(uiAtlas);
    }

    private void setAtlasFilters(@NotNull TextureAtlas atlas, @NotNull Texture.TextureFilter filter) {
        for (Texture t : atlas.getTextures()) {
            t.setFilter(filter, filter);
        }
    }

    public @NotNull AssetsGame getAssetsGame() {
        return assetsGame;
    }

    public @NotNull AssetsUI getAssetsUI() {
        return assetsUI;
    }

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error(TAG, "Can't load asset: " + asset.fileName +": " + throwable.toString());
        throw new RuntimeException("Can't load assets: ", throwable);
    }

    @Override
    public void dispose() {
        assetManager.dispose();
    }
}
