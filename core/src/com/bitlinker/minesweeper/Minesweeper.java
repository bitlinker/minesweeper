package com.bitlinker.minesweeper;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.bitlinker.minesweeper.assets.Assets;
import com.bitlinker.minesweeper.screens.GameScreen;
import com.bitlinker.minesweeper.screens.SplashScreen;
import org.jetbrains.annotations.NotNull;

import java.io.IOException;

public class Minesweeper extends ScreenManagedGame {
	private static final String TAG = "Minesweeper";
	//SpriteBatch batch;
	//Texture img;

    private com.bitlinker.minesweeper.assets.Assets assets;
    private GamePreferences gamePreferences;

    public @NotNull
    com.bitlinker.minesweeper.assets.Assets getAssets() {
        return assets;
    }


    @Override
	public void create () {
		Gdx.app.log(TAG, "Test message");

		assets = new Assets();
        try {
            assets.init();
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e); // TODO: handle properly?
        }

        Preferences preferences = Gdx.app.getPreferences("prefs.cfg");
        gamePreferences = new GamePreferences(preferences);

        Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
        Gdx.graphics.setContinuousRendering(false);
        Gdx.graphics.requestRendering();

        // TODO: debug
        //setScreen(new SplashScreen(this));
        setScreen(new GameScreen(this));
    }
}
