package com.bitlinker.minesweeper.model;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by bitlinker on 03.12.2017.
 */

public class Model {
    public static final int FLAG_BOMB = 0x1;
    public static final int FLAG_OPEN = 0x2;
    public static final int FLAG_MARK = 0x4;
    public static final int FLAG_QUESTION = 0x8;

    // For terminal game states:
    public static final int MARK_MINE_EXPLODED = 3;
    public static final int MARK_MINE_FLAG_INVALID = 4;

    private static final int[][] NEIGHBOR_INDEXES = {
            {-1, -1}, {0, -1}, {1, - 1},
            {-1, 0},  {1, 0},
            {-1, 1}, {0, 1}, {1, 1},
    };;

    // On game over:
    // - all mines are shown
    // - detonated mine is shown red
    // - flagged mines are shown as flags
    // - incorrectly flagged mines are shown as cross
    // - questions are ignored

    /**
     * Field states
     */
    private final int[] field;
    private final int[] fieldNeighbourCount;

    private final int width;
    private final int height;

    private boolean isGameOver;
    private boolean isWin;

    public Model(int width, int height, int numMines) {
        this.width = width;
        this.height = height;

        field = new int[width * height];
        fieldNeighbourCount = new int[field.length];

        setupMines(numMines);
    }

    private void setupMines(int count) {
        // Random order indexes
        List<Integer> indexes = new ArrayList<Integer>();
        for (int i = 0; i < field.length; ++i) {
            indexes.add(i);
        }
        Collections.shuffle(indexes);

        // Put mines at top N indexed cells
        for (int i = 0; i < count; ++i) {
            int index = indexes.get(i);
            field[index] |= FLAG_BOMB;

            for (int[] neighborIndex : NEIGHBOR_INDEXES) {
                incMineCount(index + neighborIndex);
            }
        }
    }

    private void incMineCount(int x, int y) {
        if (x >= 0 && x < width && y >= 0 && y < height) {
            ++fieldNeighbourCount[x + y * width];
        }
    }

    public void toggleMark(int index) {
        int curVal = field[index];
        if ((curVal & FLAG_MARK) > 0) {
            field[index] = 0;
        }

        // TODO: check game over?
    }

    public int getIndex(int x, int y) {
        return x + y * width;
    }

    public boolean isOverlayMark(int index) {
        return (checkFlag(index, FLAG_MARK));
    }

    public boolean isOverlayQuestion(int index) {
        return (checkFlag(index, FLAG_QUESTION));
    }

    public void open(int index) {
        if (isOpened(index)) { // Already opened, do nothing
            return;
        }

        if (checkFlag(index, FLAG_BOMB)) { // Explosion
            setGameOver(index);
        } else { // Fine to go
            openCellsRecursive(index);
        }
    }

    private boolean checkFlag(int index, int flag) {
        return (field[index] & flag) > 0;
    }

    private void setFlag(int index, int flag) {
        field[index] |= flag;
    }

    private void clearFlag(int index, int flag) {
        field[index] &= ~flag;
    }

    private void openCellsRecursive(int index) {
        if (!isOpened(index)) {
            setFlag(index, FLAG_OPEN);
            if (getNeighbourMineCount(index) == 0) {
                for (int neighborIndex : NEIGHBOR_INDEXES) {
                    int newIndex = index + neighborIndex;
                    if (isIndexInBounds(newIndex)) {
                        openCellsRecursive(newIndex);
                    }
                }
            }
        }
    }

    private void setGameOver(int index) {
        setFlag(index, FLAG_OPEN);
        setFlag(index, MARK_MINE_EXPLODED);
        isGameOver = true;
        // TODO: set all exploded
    }

    private boolean isIndexInBounds(int index) {
        return index >= 0 && index < field.length;
    }

    public int getNeighbourMineCount(int index) {
        return fieldNeighbourCount[index];
    }

    public boolean isOpened(int index) {
        return checkFlag(index, FLAG_OPEN);
    }

    public int getHeight() {
        return height;
    }

    public int getWidth() {
        return width;
    }

    public boolean isMine(int index) {
        return checkFlag(index, FLAG_BOMB);
    }
}
