package com.bitlinker.minesweeper.screens;

import com.badlogic.gdx.Input;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.*;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.bitlinker.minesweeper.Minesweeper;
import com.bitlinker.minesweeper.assets.AssetsGame;
import com.bitlinker.minesweeper.model.Model;
import org.jetbrains.annotations.NotNull;

public class GameScreen extends BaseSceneScreen {
    private static final float CELL_SIZE = 16.F;

    private final Minesweeper minesweeper;

    // TODO: recorder wrapper
    // TODO: clock counter
    private Model model;

    private AssetsGame assetsGame;

    private Table mainTable;

    private class MineActor extends Actor {
        private final int index;

        public MineActor(int index) {
            super();
            this.index = index;
            setSize(CELL_SIZE, CELL_SIZE);
            setTouchable(Touchable.enabled);
            addListener(new ClickListener() {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    model.open(MineActor.this.index);
                }
            });
            addListener(new ClickListener(Input.Buttons.RIGHT) {
                @Override
                public void clicked(InputEvent event, float x, float y) {
                    super.clicked(event, x, y);
                    model.toggleMark(MineActor.this.index);
                }
            });
        }

        @Override
        public void draw(Batch batch, float parentAlpha) {
            super.draw(batch, parentAlpha);

            Color color = getColor();
            batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);

            TextureRegion region = null;
            boolean opened = model.isOpened(index);

            // Opened/closed
            if (opened) {
                int neighbourMineCount = model.getNeighbourMineCount(index);
                region = assetsGame.regionCellOpened[neighbourMineCount];
            } else {
                region = assetsGame.regionCellClosed;
            }
            // Overlays
            if (model.isOverlayMark(index)) {
                region = assetsGame.regionCellFlag;
            } else if (model.isOverlayQuestion(index)) {
                region = assetsGame.regionCellQuestion;
            }

            // DBG
            int neighbourMineCount = model.getNeighbourMineCount(index);
            region = assetsGame.regionCellOpened[neighbourMineCount];

            // DBG
            if (model.isMine(index)) {
                region = assetsGame.regionMine;
            }

            batch.draw(region, getX(), getY());
        }
    }

    // TODO: model updated listener...

    public GameScreen(@NotNull Minesweeper minesweeper) {
        this.minesweeper = minesweeper;

        assetsGame = minesweeper.getAssets().getAssetsGame();

        // TODO: params
        // Beginner: 8x8, 10
        // Intermediate 16x16, 40
        // Expert 30x16, 99
        model = new Model(16, 16, 5);
    }

    @Override
    protected void rebuildStage(@NotNull Stage stage) {
        super.rebuildStage(stage);
        AssetsGame assetsGame = minesweeper.getAssets().getAssetsGame();

        stage.clear();
        //stage.setDebugAll(true);

        mainTable = new Table();
        mainTable.setFillParent(true);
        mainTable.setDebug(true);

        mainTable.add(new Image(assetsGame.borderTL)).expandX().uniformX().height(50); // TODO: custom drawable for border
        mainTable.add(new Image(assetsGame.borderTC)).expandX().uniformX().height(50);
        mainTable.add(new Image(assetsGame.borderTR)).expandX().uniformX().height(50);

        mainTable.row().expand();


        //mainTable.add(new Image());
        //mainTable.setScale(1.5F); // TODO: scale is not working
        // TODO: UI here...

        WidgetGroup gameFieldWidgetGroup = new WidgetGroup();
        gameFieldWidgetGroup.setWidth(CELL_SIZE * model.getWidth());
        gameFieldWidgetGroup.setHeight(CELL_SIZE * model.getHeight());

        mainTable.add(gameFieldWidgetGroup);

        // TODO: view hierarchy; scanling, UI, etc...
        stage.addActor(mainTable);

        for (int y = 0; y < model.getHeight(); ++y) {
            for (int x = 0; x < model.getWidth(); ++x) {
                Actor actor = new MineActor(model.getIndex(x, y));
                actor.setPosition(x * CELL_SIZE, y * CELL_SIZE);
                gameFieldWidgetGroup.addActor(actor);
            }
        }
    }
}
