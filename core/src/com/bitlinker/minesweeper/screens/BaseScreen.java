package com.bitlinker.minesweeper.screens;

import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import org.jetbrains.annotations.Nullable;

public abstract class BaseScreen implements Screen {
    @Override
    public void show() {

    }

    @Override
    public void render(float delta) {

    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }

    public @Nullable InputProcessor getInputProcessor() {
        return null;
    }
}
