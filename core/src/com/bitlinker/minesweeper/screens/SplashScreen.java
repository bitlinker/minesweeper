package com.bitlinker.minesweeper.screens;

import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.bitlinker.minesweeper.Minesweeper;
import com.bitlinker.minesweeper.assets.AssetsUI;
import org.jetbrains.annotations.NotNull;

import static com.badlogic.gdx.scenes.scene2d.actions.Actions.*;

public class SplashScreen extends BaseSceneScreen {
    private static final float DELAY_TIME = 2.5F;
    private static final float FADEIN_TITLE_TIME = 1.0F;
    private static final float FADEIN_BG_DELAY_TIME = 0.5F;
    private static final float FADEIN_BG_TIME = 0.5F;
    private static final float FADEOUT_TIME = 0.5F;

    private final Minesweeper minesweeper;

    public SplashScreen(@NotNull Minesweeper minesweeper) {
        this.minesweeper = minesweeper;
    }

    private class CompleteAction extends Action {
        @Override
        public boolean act(float delta) {
            minesweeper.setScreen(new MainMenuScreen());
            return true;
        }
    }

    @Override
    protected void rebuildStage(@NotNull Stage stage) {
        stage.clear();
        stage.setDebugAll(true);
        AssetsUI assetsUI = minesweeper.getAssets().getAssetsUI();
        Image bg = new Image(assetsUI.getSplashBgRegion());
        Image title = new Image(assetsUI.getSplashTitleRegion());
        bg.setColor(1.F, 1.F, 1.F, 0.F);
        title.setColor(1.F, 1.F, 1.F, 0.F);
        title.setPosition(
                (stage.getWidth() - title.getWidth()) * 0.5F,
                (stage.getHeight() - title.getHeight()) * 0.5F
        );

        stage.addActor(bg);
        stage.addActor(title);

        SequenceAction bgAction = new SequenceAction(
                delay(FADEIN_BG_DELAY_TIME),
                fadeIn(FADEIN_BG_TIME),
                delay(DELAY_TIME),
                fadeOut(FADEOUT_TIME),
                new CompleteAction()
        );
        bg.addAction(bgAction);

        SequenceAction titleAction = new SequenceAction(
                fadeIn(FADEIN_TITLE_TIME),
                delay(DELAY_TIME),
                fadeOut(FADEOUT_TIME)
        );
        title.addAction(titleAction);
    }
}
