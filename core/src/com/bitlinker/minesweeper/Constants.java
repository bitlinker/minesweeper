package com.bitlinker.minesweeper;

public class Constants {
    public static final float VIEWPORT_GUI_WIDTH = 1280.0f;
    public static final float VIEWPORT_GUI_HEIGHT = 720.0f;
}
