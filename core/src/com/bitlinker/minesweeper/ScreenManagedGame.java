package com.bitlinker.minesweeper;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.bitlinker.minesweeper.screens.BaseScreen;
import org.jetbrains.annotations.NotNull;

public class ScreenManagedGame extends ApplicationAdapter {
    private com.bitlinker.minesweeper.screens.BaseScreen screen;
    private int width, height;

    public void setScreen(@NotNull BaseScreen screen) {
        if (this.screen != null) {
            this.screen.hide();
        }
        this.screen = screen;
        screen.show();
        screen.resize(width, height);
        Gdx.input.setInputProcessor(screen.getInputProcessor());
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        this.width = width;
        this.height = height;
        if (screen != null) {
            screen.resize(width, height);
        }
    }

    @Override
    public void render() {
        super.render();
        if (screen != null) {
            float dt = Math.min(Gdx.graphics.getDeltaTime(), 1.0f / 60.0f);
            Gdx.app.log("render", "render");
            screen.render(dt);
        }
    }

    @Override
    public void pause() {
        super.pause();
        if (screen != null) {
            screen.pause();
        }
    }

    @Override
    public void resume() {
        super.resume();
        if (screen != null) {
            screen.resume();
        }
    }

    @Override
    public void dispose() {
        super.dispose();
        if (screen != null) {
            screen.dispose();
            screen = null;
        }
    }
}
