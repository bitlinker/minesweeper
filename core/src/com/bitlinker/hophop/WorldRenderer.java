package com.bitlinker.hophop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.GdxRuntimeException;

import org.jetbrains.annotations.NotNull;

/**
 * Created by bitlinker on 18.11.2017.
 */

public class WorldRenderer implements Disposable {
    private OrthographicCamera mCamera;
    private OrthographicCamera mCameraGUI;
    private SpriteBatch mSpriteBatch;
    private WorldController mWorldController;
    private static final boolean DEBUG_DRAW_BOX2D_WORLD = false;
    private Box2DDebugRenderer b2debugRenderer;
    private ShaderProgram shaderMonochrome;

    public WorldRenderer(@NotNull WorldController controller) {
        mWorldController = controller;
        init();
    }

    private void init() {
        mSpriteBatch = new SpriteBatch();
        mCamera = new OrthographicCamera(Constants.VIEWPORT_WIDTH, Constants.VIEWPORT_HEIGHT);
        mCamera.position.set(0, 0, 0);
        mCamera.update();
        mCameraGUI = new OrthographicCamera(
                Constants.VIEWPORT_GUI_WIDTH,
                Constants.VIEWPORT_GUI_HEIGHT);
        mCameraGUI.position.set(0, 0, 0);
        mCameraGUI.setToOrtho(true);
        mCameraGUI.update();
        b2debugRenderer = new Box2DDebugRenderer();
        shaderMonochrome = new ShaderProgram(
                Gdx.files.internal(Constants.shaderMonochromeVertex),
                Gdx.files.internal(Constants.shaderMonochromeFragment));
        if (!shaderMonochrome.isCompiled()) {
            String msg = "Could not compile shader program: "
                    + shaderMonochrome.getLog();
            throw new GdxRuntimeException(msg);
        }
    }

    public void render() {
        renderWorld(mSpriteBatch);
        renderGui(mSpriteBatch);
    }

    private void renderWorld(@NotNull SpriteBatch batch) {
        mWorldController.mCameraHelper.applyTo(mCamera);
        batch.setProjectionMatrix(mCamera.combined);
        batch.begin();
        if (GamePreferences.instance.useMonochromeShader) {
            batch.setShader(shaderMonochrome);
            shaderMonochrome.setUniformf("u_amount", 1.0f);
        }
        mWorldController.mLevel.render(batch);
        batch.setShader(null);
        batch.end();
        if (DEBUG_DRAW_BOX2D_WORLD) {
            b2debugRenderer.render(mWorldController.b2world, mCamera.combined);
        }
    }

    private void renderGui(@NotNull SpriteBatch batch) {
        batch.setProjectionMatrix(mCameraGUI.combined);
        batch.begin();
        renderGuiScore(batch);
        renderGuiFeatherPowerup(batch);
        renderGuiExtraLive(batch);
        if (GamePreferences.instance.showFpsCounter)
            renderGuiFpsCounter(batch);
        renderGuiGameOverMessage(batch);
        batch.end();
    }

    private void renderGuiScore(@NotNull SpriteBatch batch) {
        float x = -15;
        float y = -15;
        float offsetX = 50;
        float offsetY = 50;
        if (mWorldController.scoreVisual < mWorldController.mScore) {
            long shakeAlpha = System.currentTimeMillis() % 360;
            float shakeDist = 1.5f;
            offsetX += MathUtils.sinDeg(shakeAlpha * 2.2f) * shakeDist;
            offsetY += MathUtils.sinDeg(shakeAlpha * 2.9f) * shakeDist;
        }
        batch.draw(Assets.sInstance.mGoldCoin.mGoldCoin, x, y, offsetX,
                offsetY, 100, 100, 0.35f, -0.35f, 0);
        Assets.sInstance.mFonts.mDefaultBig.draw(batch,
                "" + (int) mWorldController.scoreVisual,
                x + 75, y + 37);
    }

    private void renderGuiExtraLive(@NotNull SpriteBatch batch) {
        float x = mCameraGUI.viewportWidth - 50 - Constants.LIVES_START * 50;
        float y = -15;
        for (int i = 0; i < Constants.LIVES_START; i++) {
            if (mWorldController.mLives <= i)
                batch.setColor(0.5f, 0.5f, 0.5f, 0.5f);
            batch.draw(Assets.sInstance.mBunny.mHead,
                    x + i * 50, y, 50, 50, 120, 100, 0.35f, -0.35f, 0);
            batch.setColor(1, 1, 1, 1);
        }
        if (mWorldController.mLives >= 0 && mWorldController.livesVisual > mWorldController.mLives) {
            int i = mWorldController.mLives;
            float alphaColor = Math.max(0, mWorldController.livesVisual - mWorldController.mLives - 0.5f);
            float alphaScale = 0.35f * (2 + mWorldController.mLives - mWorldController.livesVisual) * 2;
            float alphaRotate = -45 * alphaColor;
            batch.setColor(1.0f, 0.7f, 0.7f, alphaColor);
            batch.draw(Assets.sInstance.mBunny.mHead, x + i * 50, y, 50, 50, 120, 100, alphaScale, -alphaScale, alphaRotate);
            batch.setColor(1, 1, 1, 1);
        }
    }

    private void renderGuiFpsCounter(@NotNull SpriteBatch batch) {
        float x = mCameraGUI.viewportWidth - 55;
        float y = mCameraGUI.viewportHeight - 15;
        int fps = Gdx.graphics.getFramesPerSecond();
        BitmapFont fpsFont = Assets.sInstance.mFonts.mDefaultNormal;
        if (fps >= 45) {
            fpsFont.setColor(0, 1, 0, 1);
        } else if (fps >= 30) {
            fpsFont.setColor(1, 1, 0, 1);
        } else {
            fpsFont.setColor(1, 0, 0, 1);
        }
        fpsFont.draw(batch, "FPS: " + fps, x, y);
        fpsFont.setColor(1, 1, 1, 1); // white
    }

    private void renderGuiGameOverMessage(@NotNull SpriteBatch batch) {
        float x = mCameraGUI.viewportWidth / 2;
        float y = mCameraGUI.viewportHeight / 2;
        if (mWorldController.isGameOver()) {
            BitmapFont fontGameOver = Assets.sInstance.mFonts.mDefaultBig;
            fontGameOver.setColor(1, 0.75f, 0.25f, 1);
            fontGameOver.draw(batch, "GAME OVER", x, y);
            fontGameOver.setColor(1, 1, 1, 1);
        }
    }

    private void renderGuiFeatherPowerup(@NotNull SpriteBatch batch) {
        float x = -15;
        float y = 30;
        float timeLeftFeatherPowerup = mWorldController.mLevel.mBunnyHead.mTimeLeftFeatherPowerup;
        if (timeLeftFeatherPowerup > 0) {
            // Start icon fade in/out if the left power-up time
            // is less than 4 seconds. The fade interval is set
            // to 5 changes per second.
            if (timeLeftFeatherPowerup < 4) {
                if (((int) (timeLeftFeatherPowerup * 5) % 2) != 0) {
                    batch.setColor(1, 1, 1, 0.5f);
                }
            }
            batch.draw(Assets.sInstance.mFeather.mFeather,
                    x, y, 50, 50, 100, 100, 0.35f, -0.35f, 0);
            batch.setColor(1, 1, 1, 1);
            Assets.sInstance.mFonts.mDefaultSmall.draw(batch,
                    "" + (int) timeLeftFeatherPowerup, x + 60, y + 57);
        }
    }

    public void resize(int width, int height) {
        mCamera.viewportWidth = (Constants.VIEWPORT_HEIGHT / height) * width;
        mCamera.update();

        mCameraGUI.viewportHeight = Constants.VIEWPORT_GUI_HEIGHT;
        mCameraGUI.viewportWidth = (Constants.VIEWPORT_GUI_HEIGHT / (float) height) * (float) width;
        mCameraGUI.position.set(
                mCameraGUI.viewportWidth / 2,
                mCameraGUI.viewportHeight / 2,
                0);
        mCameraGUI.update();
    }

    @Override
    public void dispose() {
        mSpriteBatch.dispose();
        shaderMonochrome.dispose();
    }
}
