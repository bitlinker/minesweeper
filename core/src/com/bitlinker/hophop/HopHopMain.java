package com.bitlinker.hophop;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.math.Interpolation;
import com.bitlinker.hophop.screens.MenuScreen;
import com.bitlinker.hophop.screens.ScreenTransition;
import com.bitlinker.hophop.screens.ScreenTransitionSlice;

/**
 * Created by bitlinker on 18.11.2017.
 */

public class HopHopMain extends DirectedGame {
    @Override
    public void create() {
        Gdx.app.setLogLevel(Application.LOG_DEBUG);
        Assets.sInstance.init(new AssetManager());
        GamePreferences.instance.load();
        AudioManager.instance.play(Assets.sInstance.music.song01);
        ScreenTransition transition = ScreenTransitionSlice.init(2, ScreenTransitionSlice.UP_DOWN, 10, Interpolation.pow5Out);
        setScreen(new MenuScreen(this), transition);
    }
}
