package com.bitlinker.hophop.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.GL20;
import com.bitlinker.hophop.DirectedGame;
import com.bitlinker.hophop.GamePreferences;
import com.bitlinker.hophop.WorldController;
import com.bitlinker.hophop.WorldRenderer;

import org.jetbrains.annotations.NotNull;

/**
 * Created by bitlinker on 19.11.2017.
 */

public class GameScreen extends AbstractGameScreen {
    private static final String TAG = GameScreen.class.getName();
    private WorldController mWorldController;
    private WorldRenderer mWorldRenderer;
    private boolean mIsPaused;

    public GameScreen(@NotNull DirectedGame game) {
        super(game);
    }

    @Override
    public void render(float deltaTime) {
        if (!mIsPaused) {
            mWorldController.update(deltaTime);
        }

        Gdx.gl.glClearColor(0x64 / 255.0f, 0x95 / 255.0f, 0xed / 255.0f, 0xff / 255.0f);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        mWorldRenderer.render();
    }

    @Override
    public void resize(int width, int height) {
        mWorldRenderer.resize(width, height);
    }

    @Override
    public void show() {
        GamePreferences.instance.load();
        mWorldController = new WorldController(game);
        mWorldRenderer = new WorldRenderer(mWorldController);
        Gdx.input.setCatchBackKey(true);
    }

    @Override
    public void hide() {
        mWorldRenderer.dispose();
        mWorldController.dispose();
        Gdx.input.setCatchBackKey(false);
    }

    @Override
    public void pause() {
        mIsPaused = true;
    }

    @Override
    public void resume() {
        super.resume();
        mIsPaused = false;
    }

    @Override
    public InputProcessor getInputProcessor () {
        return mWorldController;
    }
}
