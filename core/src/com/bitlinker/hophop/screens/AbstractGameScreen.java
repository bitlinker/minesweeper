package com.bitlinker.hophop.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.assets.AssetManager;
import com.bitlinker.hophop.Assets;
import com.bitlinker.hophop.DirectedGame;
import org.jetbrains.annotations.NotNull;


/**
 * Created by bitlinker on 19.11.2017.
 */

public abstract class AbstractGameScreen implements Screen {
    protected DirectedGame game;
    public AbstractGameScreen (@NotNull DirectedGame game) {
        this.game = game;
    }
    public abstract void render (float deltaTime);
    public abstract void resize (int width, int height);
    public abstract void show ();
    public abstract void hide ();
    public abstract void pause ();

    public void resume () {
        Assets.sInstance.init(new AssetManager());
    }
    public void dispose () {
        Assets.sInstance.dispose();
    }

    public abstract InputProcessor getInputProcessor ();
}
