package com.bitlinker.hophop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.bitlinker.hophop.objects.AbstractGameObject;
import com.bitlinker.hophop.objects.BunnyHead;
import com.bitlinker.hophop.objects.Carrot;
import com.bitlinker.hophop.objects.Clouds;
import com.bitlinker.hophop.objects.Feather;
import com.bitlinker.hophop.objects.Goal;
import com.bitlinker.hophop.objects.GoldCoin;
import com.bitlinker.hophop.objects.Mountains;
import com.bitlinker.hophop.objects.Rock;
import com.bitlinker.hophop.objects.WaterOverlay;
import org.jetbrains.annotations.NotNull;

/**
 * Created by bitlinker on 18.11.2017.
 */

public class Level {
    private static final String TAG = "Level";

    public enum BLOCK_TYPE {
        EMPTY(0, 0, 0), // black
        ROCK(0, 255, 0), // green
        PLAYER_SPAWNPOINT(255, 255, 255), // white
        GOAL(255, 0, 0), // red
        ITEM_FEATHER(255, 0, 255), // purple
        ITEM_GOLD_COIN(255, 255, 0); // yellow
        private int mColor;

        private BLOCK_TYPE(int r, int g, int b) {
            mColor = r << 24 | g << 16 | b << 8 | 0xff;
        }

        public boolean sameColor(int color) {
            return this.mColor == color;
        }

        public int getColor() {
            return mColor;
        }
    }

    // objects
    public BunnyHead mBunnyHead;
    public Array<GoldCoin> mGoldCoins;
    public Array<Feather> mFeathers;
    public Array<Rock> mRocks;
    public Array<Carrot> carrots;
    public Goal goal;

    // decoration
    public Clouds mClouds;
    public Mountains mMountains;
    public WaterOverlay mWaterOverlay;

    public Level(@NotNull String filename) {
        init(filename);
    }

    private void init(@NotNull String filename) {
        mBunnyHead = null;
        mRocks = new Array<Rock>();
        mGoldCoins = new Array<GoldCoin>();
        mFeathers = new Array<Feather>();
        carrots = new Array<Carrot>();

        Pixmap pixmap = new Pixmap(Gdx.files.internal(filename));

        int lastPixel = -1;
        for (int pixelY = 0; pixelY < pixmap.getHeight(); pixelY++) {
            for (int pixelX = 0; pixelX < pixmap.getWidth(); pixelX++) {
                AbstractGameObject obj;
                float offsetHeight;
                float baseHeight = pixmap.getHeight() - pixelY;
                int currentPixel = pixmap.getPixel(pixelX, pixelY);
                if (BLOCK_TYPE.EMPTY.sameColor(currentPixel)) {
                    // do nothing
                } else if (BLOCK_TYPE.ROCK.sameColor(currentPixel)) {
                    if (lastPixel != currentPixel) {
                        obj = new Rock();
                        float heightIncreaseFactor = 0.25f;
                        offsetHeight = -2.5f;
                        obj.mPosition.set(
                                pixelX,
                                baseHeight * obj.mDimension.y * heightIncreaseFactor + offsetHeight
                        );
                        mRocks.add((Rock) obj);
                    } else {
                        mRocks.get(mRocks.size - 1).increaseLength(1);
                    }
                } else if (BLOCK_TYPE.PLAYER_SPAWNPOINT.sameColor(currentPixel)) {
                    obj = new BunnyHead();
                    offsetHeight = -3.0f;
                    obj.mPosition.set(pixelX, baseHeight * obj.mDimension.y + offsetHeight);
                    mBunnyHead = (BunnyHead) obj;
                } else if (BLOCK_TYPE.ITEM_FEATHER.sameColor(currentPixel)) {
                    obj = new Feather();
                    offsetHeight = -1.5f;
                    obj.mPosition.set(pixelX, baseHeight * obj.mDimension.y + offsetHeight);
                    mFeathers.add((Feather) obj);
                } else if (BLOCK_TYPE.ITEM_GOLD_COIN.sameColor(currentPixel)) {
                    obj = new GoldCoin();
                    offsetHeight = -1.5f;
                    obj.mPosition.set(pixelX, baseHeight * obj.mDimension.y + offsetHeight);
                    mGoldCoins.add((GoldCoin) obj);
                } else if (BLOCK_TYPE.GOAL.sameColor(currentPixel)) {
                    obj = new Goal();
                    offsetHeight = -7.0f;
                    obj.mPosition.set(pixelX, baseHeight + offsetHeight);
                    goal = (Goal) obj;
                } else {
                    int r = 0xff & (currentPixel >>> 24); //red color channel
                    int g = 0xff & (currentPixel >>> 16); //green color channel
                    int b = 0xff & (currentPixel >>> 8); //blue color channel
                    int a = 0xff & currentPixel; //alpha channel
                    Gdx.app.error(TAG, "Unknown object at x<" + pixelX + "> y<" + pixelY + ">: r<" + r + "> g<" + g + "> b<" + b + "> a<" + a + ">");
                }
                lastPixel = currentPixel;
            }
        }

        mClouds = new Clouds(pixmap.getWidth());
        mClouds.mPosition.set(0, 2);
        mMountains = new Mountains(pixmap.getWidth());
        mMountains.mPosition.set(-1, -1);
        mWaterOverlay = new WaterOverlay(pixmap.getWidth());
        mWaterOverlay.mPosition.set(0, -3.75f);

        pixmap.dispose();
        Gdx.app.debug(TAG, "mLevel '" + filename + "' loaded");
    }

    public void render(@NotNull SpriteBatch batch) {
        mMountains.render(batch);
        goal.render(batch);
        for (Rock rock : mRocks)
            rock.render(batch);
        for (GoldCoin goldCoin : mGoldCoins)
            goldCoin.render(batch);
        for (Feather feather : mFeathers)
            feather.render(batch);
        for (Carrot carrot : carrots)
            carrot.render(batch);
        mBunnyHead.render(batch);
        mWaterOverlay.render(batch);
        mClouds.render(batch);
    }

    public void update(float deltaTime) {
        mBunnyHead.update(deltaTime);
        for (Rock rock : mRocks)
            rock.update(deltaTime);
        for (GoldCoin goldCoin : mGoldCoins)
            goldCoin.update(deltaTime);
        for (Feather feather : mFeathers)
            feather.update(deltaTime);
        for (Carrot carrot : carrots)
            carrot.update(deltaTime);
        mClouds.update(deltaTime);
    }
}
