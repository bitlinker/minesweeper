package com.bitlinker.hophop;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetDescriptor;
import com.badlogic.gdx.assets.AssetErrorListener;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.utils.Disposable;

/**
 * Created by bitlinker on 18.11.2017.
 */

public class Assets implements Disposable, AssetErrorListener {

    public class AssetBunny {
        public final TextureAtlas.AtlasRegion mHead;

        public AssetBunny(TextureAtlas atlas) {
            mHead = atlas.findRegion("bunny_head");
        }
    }

    public class AssetRock {
        public final TextureAtlas.AtlasRegion mEdge;
        public final TextureAtlas.AtlasRegion mMidle;

        public AssetRock(TextureAtlas atlas) {
            mEdge = atlas.findRegion("rock_edge");
            mMidle = atlas.findRegion("rock_middle");
        }
    }

    public class AssetGoldCoin {
        public final TextureAtlas.AtlasRegion mGoldCoin;

        public AssetGoldCoin(TextureAtlas atlas) {
            mGoldCoin = atlas.findRegion("item_gold_coin");
        }
    }

    public class AssetFeather {
        public final TextureAtlas.AtlasRegion mFeather;

        public AssetFeather(TextureAtlas atlas) {
            mFeather = atlas.findRegion("item_feather");
        }
    }

    public class AssetLevelDecoration {
        public final TextureAtlas.AtlasRegion mCloud01;
        public final TextureAtlas.AtlasRegion mCloud02;
        public final TextureAtlas.AtlasRegion mCloud03;
        public final TextureAtlas.AtlasRegion mMountainLeft;
        public final TextureAtlas.AtlasRegion mMountainRight;
        public final TextureAtlas.AtlasRegion mWaterOverlay;

        public final TextureAtlas.AtlasRegion carrot;
        public final TextureAtlas.AtlasRegion goal;

        public AssetLevelDecoration(TextureAtlas atlas) {
            mCloud01 = atlas.findRegion("cloud01");
            mCloud02 = atlas.findRegion("cloud02");
            mCloud03 = atlas.findRegion("cloud03");
            mMountainLeft = atlas.findRegion("mountain_left");
            mMountainRight = atlas.findRegion("mountain_right");
            mWaterOverlay = atlas.findRegion("water_overlay");
            carrot = atlas.findRegion("carrot");
            goal = atlas.findRegion("goal");
        }
    }

    public class AssetFonts implements Disposable {
        public final BitmapFont mDefaultSmall;
        public final BitmapFont mDefaultNormal;
        public final BitmapFont mDefaultBig;

        public AssetFonts() {
            mDefaultSmall = new BitmapFont(
                    Gdx.files.internal("images/arial-15.fnt"), true);
            mDefaultNormal = new BitmapFont(
                    Gdx.files.internal("images/arial-15.fnt"), true);
            mDefaultBig = new BitmapFont(
                    Gdx.files.internal("images/arial-15.fnt"), true);

            mDefaultSmall.getRegion().getTexture().setFilter(
                    Texture.TextureFilter.Linear,
                    Texture.TextureFilter.Linear);
            mDefaultNormal.getRegion().getTexture().setFilter(
                    Texture.TextureFilter.Linear,
                    Texture.TextureFilter.Linear);
            mDefaultBig.getRegion().getTexture().setFilter(
                    Texture.TextureFilter.Linear,
                    Texture.TextureFilter.Linear);
        }

        @Override
        public void dispose() {
            mDefaultBig.dispose();
            mDefaultNormal.dispose();
            mDefaultSmall.dispose();
        }
    }

    public class AssetSounds {
        public final Sound jump;
        public final Sound jumpWithFeather;
        public final Sound pickupCoin;
        public final Sound pickupFeather;
        public final Sound liveLost;
        public AssetSounds (AssetManager am) {
            jump = am.get("sounds/jump.wav", Sound.class);
            jumpWithFeather = am.get("sounds/jump_with_feather.wav", Sound.class);
            pickupCoin = am.get("sounds/pickup_coin.wav", Sound.class);
            pickupFeather = am.get("sounds/pickup_feather.wav", Sound.class);
            liveLost = am.get("sounds/live_lost.wav", Sound.class);
        }
    }
    public class AssetMusic {
        public final Music song01;
        public AssetMusic (AssetManager am) {
            song01 = am.get("music/bgm.mp3", Music.class);
        }
    }

    public static final Assets sInstance = new Assets();
    private static final String TAG = "Assets";
    private AssetManager mAssetManager;

    public AssetBunny mBunny;
    public AssetRock mRock;
    public AssetGoldCoin mGoldCoin;
    public AssetFeather mFeather;
    public AssetLevelDecoration mLevelDecoration;
    public AssetFonts mFonts;

    public AssetSounds sounds;
    public AssetMusic music;

    private Assets() {
    }

    public void init(AssetManager assetManager) {
        mAssetManager = assetManager;
        assetManager.setErrorListener(this);
        assetManager.load(Constants.TEXTURE_ATLAS_OBJECTS, TextureAtlas.class);
        assetManager.load("sounds/jump.wav", Sound.class);
        assetManager.load("sounds/jump_with_feather.wav", Sound.class);
        assetManager.load("sounds/pickup_coin.wav", Sound.class);
        assetManager.load("sounds/pickup_feather.wav", Sound.class);
        assetManager.load("sounds/live_lost.wav", Sound.class);
        assetManager.load("music/bgm.mp3", Music.class);
        assetManager.finishLoading();
        Gdx.app.debug(TAG, "# of assets loaded: " + assetManager.getAssetNames().size);

        TextureAtlas atlas = assetManager.get(Constants.TEXTURE_ATLAS_OBJECTS);

        for (Texture t : atlas.getTextures()) {
            t.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        }
        mBunny = new AssetBunny(atlas);
        mRock = new AssetRock(atlas);
        mGoldCoin = new AssetGoldCoin(atlas);
        mFeather = new AssetFeather(atlas);
        mLevelDecoration = new AssetLevelDecoration(atlas);
        mFonts = new AssetFonts();

        sounds = new AssetSounds(assetManager);
        music = new AssetMusic(assetManager);
    }

    @Override
    public void error(AssetDescriptor asset, Throwable throwable) {
        Gdx.app.error(TAG, "Couldn't load asset '" + asset.fileName + "'", (Exception) throwable);
    }

    @Override
    public void dispose() {
        mAssetManager.dispose();
        mFonts.dispose();
    }
}
