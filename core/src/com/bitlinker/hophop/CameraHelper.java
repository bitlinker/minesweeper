package com.bitlinker.hophop;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.bitlinker.hophop.objects.AbstractGameObject;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

/**
 * Created by bitlinker on 18.11.2017.
 */

public class CameraHelper {
    private final float MAX_ZOOM_IN = 0.25f;
    private final float MAX_ZOOM_OUT = 10.0f;

    private final float FOLLOW_SPEED = 4.0f;

    private Vector2 mPosition;
    private float mZoom;
    private AbstractGameObject mTarget;

    public CameraHelper() {
        mPosition = new Vector2();
        mZoom = 1.F;
    }

    public void update(float dt) {
        if (!hasTarget()) return;
        mPosition.lerp(mTarget.mPosition, FOLLOW_SPEED * dt);

        // Prevent camera from moving down too far
        mPosition.y = Math.max(-1f, mPosition.y);
    }

    public void setPosition(float x, float y) {
        mPosition.set(x, y);
    }

    public @NotNull
    Vector2 getPosition() {
        return mPosition;
    }

    public void addZoom(float amount) {
        setZoom(mZoom + amount);
    }

    public void setZoom(float zoom) {
        mZoom = MathUtils.clamp(zoom, MAX_ZOOM_IN, MAX_ZOOM_OUT);
    }

    public float getZoom() {
        return mZoom;
    }

    public void setTarget(@Nullable AbstractGameObject target) {
        mTarget = target;
    }

    public @NotNull AbstractGameObject getTarget() {
        return mTarget;
    }

    public boolean hasTarget() {
        return mTarget != null;
    }

    public boolean hasTarget(@Nullable AbstractGameObject target) {
        return hasTarget() && mTarget.equals(target);
    }

    public void applyTo(@NotNull OrthographicCamera camera) {
        camera.position.x = mPosition.x;
        camera.position.y = mPosition.y;
        camera.zoom = mZoom;
        camera.update();
    }
}
