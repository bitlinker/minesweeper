package com.bitlinker.hophop;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Disposable;
import com.bitlinker.hophop.objects.BunnyHead;
import com.bitlinker.hophop.objects.Carrot;
import com.bitlinker.hophop.objects.Feather;
import com.bitlinker.hophop.objects.GoldCoin;
import com.bitlinker.hophop.objects.Rock;
import com.bitlinker.hophop.screens.MenuScreen;
import com.bitlinker.hophop.screens.ScreenTransition;
import com.bitlinker.hophop.screens.ScreenTransitionSlide;

import org.jetbrains.annotations.NotNull;

/**
 * Created by bitlinker on 18.11.2017.
 */

public class WorldController extends InputAdapter implements Disposable {
    private static final String TAG = "WorldController";

    private DirectedGame mGame;

    public CameraHelper mCameraHelper;

    public Level mLevel;
    public int mLives;
    public int mScore;

    public float livesVisual;
    public float scoreVisual;

    private boolean goalReached;
    public World b2world;

    private boolean accelerometerAvailable;

    public WorldController(@NotNull DirectedGame game) {
        mGame = game;
        init();
    }

    private void init() {
        accelerometerAvailable = Gdx.input.isPeripheralAvailable(Input.Peripheral.Accelerometer);
        mCameraHelper = new CameraHelper();
        mLives = Constants.LIVES_START;
        livesVisual = mLives;
        timeLeftGameOverDelay = 0;
        initLevel();
    }

    private void initLevel() {
        mScore = 0;
        scoreVisual = mScore;
        mLevel = new Level(Constants.LEVEL_01);
        mCameraHelper.setTarget(mLevel.mBunnyHead);
        initPhysics();
    }

    private void initPhysics () {
        if (b2world != null) b2world.dispose();
        b2world = new World(new Vector2(0, -9.81f), true);

        Vector2 origin = new Vector2();
        for (Rock rock : mLevel.mRocks) {
            BodyDef bodyDef = new BodyDef();
            bodyDef.type = BodyDef.BodyType.KinematicBody;
            bodyDef.position.set(rock.mPosition);
            Body body = b2world.createBody(bodyDef);
            rock.body = body;
            PolygonShape polygonShape = new PolygonShape();
            origin.x = rock.mBounds.width / 2.0f;
            origin.y = rock.mBounds.height / 2.0f;
            polygonShape.setAsBox(rock.mBounds.width / 2.0f,
                    rock.mBounds.height / 2.0f, origin, 0);
            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = polygonShape;
            body.createFixture(fixtureDef);
            polygonShape.dispose();
        }
    }

    private void spawnCarrots (Vector2 pos, int numCarrots, float radius) {
        float carrotShapeScale = 0.5f;
        for (int i = 0; i<numCarrots; i++) {
            Carrot carrot = new Carrot();
            float x = MathUtils.random(-radius, radius);
            float y = MathUtils.random(5.0f, 15.0f);
            float rotation = MathUtils.random(0.0f, 360.0f)
                    * MathUtils.degreesToRadians;
            float carrotScale = MathUtils.random(0.5f, 1.5f);
            carrot.mScale.set(carrotScale, carrotScale);
            BodyDef bodyDef = new BodyDef();
            bodyDef.position.set(pos);
            bodyDef.position.add(x, y);
            bodyDef.angle = rotation;
            Body body = b2world.createBody(bodyDef);
            body.setType(BodyDef.BodyType.DynamicBody);
            carrot.body = body;

            PolygonShape polygonShape = new PolygonShape();
            float halfWidth = carrot.mBounds.width / 2.0f * carrotScale;
            float halfHeight = carrot.mBounds.height /2.0f * carrotScale;
            polygonShape.setAsBox(halfWidth * carrotShapeScale,
                    halfHeight * carrotShapeScale);

            FixtureDef fixtureDef = new FixtureDef();
            fixtureDef.shape = polygonShape;
            fixtureDef.density = 50;
            fixtureDef.restitution = 0.5f;
            fixtureDef.friction = 0.5f;
            body.createFixture(fixtureDef);
            polygonShape.dispose();

            mLevel.carrots.add(carrot);
        }
    }

    private void backToMenu () {
        ScreenTransition transition = ScreenTransitionSlide.init(0.75f, ScreenTransitionSlide.DOWN, false, Interpolation.bounceOut);
        mGame.setScreen(new MenuScreen(mGame), transition);
    }

    @Override
    public boolean keyUp(int keycode) {
        if (keycode == Input.Keys.R) {
            init();
            Gdx.app.debug(TAG, "Game world reseted");
        } else if (keycode == Input.Keys.ENTER) {
            mCameraHelper.setTarget(mCameraHelper.hasTarget() ? null : mLevel.mBunnyHead);
            Gdx.app.debug(TAG, "Camera follow enabled: " + mCameraHelper.hasTarget());
        }
        else if (keycode == Input.Keys.ESCAPE || keycode == Input.Keys.BACK) {
            backToMenu();
        }
        return false;
    }

    public void update(float dt) {
        handleDebugInput(dt);
        if (isGameOver()) {
            timeLeftGameOverDelay -= dt;
            if (timeLeftGameOverDelay < 0) backToMenu();
        } else {
            handleInputGame(dt);
        }
        mLevel.update(dt);
        testCollisions();
        b2world.step(dt, 8, 3);
        mCameraHelper.update(dt);
        if (!isGameOver() && isPlayerInWater()) {
            AudioManager.instance.play(Assets.sInstance.sounds.liveLost);
            mLives--;
            if (isGameOver())
                timeLeftGameOverDelay = Constants.TIME_DELAY_GAME_OVER;
            else
                initLevel();
        }
        mLevel.mMountains.updateScrollPosition(mCameraHelper.getPosition());
        if (livesVisual> mLives)
            livesVisual = Math.max(mLives, livesVisual - 1 * dt);
        if (scoreVisual< mScore)
            scoreVisual = Math.min(mScore, scoreVisual + 250 * dt);
    }

    private void handleInputGame(float dt) {
        if (mCameraHelper.hasTarget(mLevel.mBunnyHead)) {
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT)) {
                mLevel.mBunnyHead.mVelocity.x = -mLevel.mBunnyHead.mTerminalVelocity.x;
            } else if (Gdx.input.isKeyPressed(Input.Keys.RIGHT)) {
                mLevel.mBunnyHead.mVelocity.x = mLevel.mBunnyHead.mTerminalVelocity.x;
            } else {
                if (accelerometerAvailable) {
                    // normalize accelerometer values from [-10, 10] to [-1, 1]
                    // which translate to rotations of [-90, 90] degrees
                    float amount = Gdx.input.getAccelerometerY() / 10.0f;
                    amount *= 90.0f;
                    // is angle of rotation inside dead zone?
                    if (Math.abs(amount) <Constants.ACCEL_ANGLE_DEAD_ZONE) {
                        amount = 0;
                    } else {
                        // use the defined max angle of rotation instead of
                        // the full 90 degrees for maximum velocity
                        amount /= Constants.ACCEL_MAX_ANGLE_MAX_MOVEMENT;
                    }
                    mLevel.mBunnyHead.mVelocity.x = mLevel.mBunnyHead.mTerminalVelocity.x * amount;
                }
                if (Gdx.app.getType() != Application.ApplicationType.Desktop) {
                    mLevel.mBunnyHead.mVelocity.x = mLevel.mBunnyHead.mTerminalVelocity.x;
                }
            }

            if (Gdx.input.isTouched() || Gdx.input.isKeyPressed(Input.Keys.SPACE)) {
                mLevel.mBunnyHead.setJumping(true);
            } else {
                mLevel.mBunnyHead.setJumping(false);
            }
        }
    }

    private void handleDebugInput(float deltaTime) {
        if (Gdx.app.getType() != Application.ApplicationType.Desktop) return;

        if (!mCameraHelper.hasTarget(mLevel.mBunnyHead)) {
            // Camera Controls (move)
            float camMoveSpeed = 5 * deltaTime;
            float camMoveSpeedAccelerationFactor = 5;
            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT))
                camMoveSpeed *= camMoveSpeedAccelerationFactor;
            if (Gdx.input.isKeyPressed(Input.Keys.LEFT))
                moveCamera(-camMoveSpeed, 0);
            if (Gdx.input.isKeyPressed(Input.Keys.RIGHT))
                moveCamera(camMoveSpeed, 0);
            if (Gdx.input.isKeyPressed(Input.Keys.UP))
                moveCamera(0, camMoveSpeed);
            if (Gdx.input.isKeyPressed(Input.Keys.DOWN))
                moveCamera(0, -camMoveSpeed);
            if (Gdx.input.isKeyPressed(Input.Keys.BACKSPACE))
                mCameraHelper.setPosition(0, 0);

            float camZoomSpeed = 1 * deltaTime;
            float camZoomSpeedAccelerationFactor = 5;
            if (Gdx.input.isKeyPressed(Input.Keys.SHIFT_LEFT))
                camZoomSpeed *= camZoomSpeedAccelerationFactor;
            if (Gdx.input.isKeyPressed(Input.Keys.COMMA))
                mCameraHelper.addZoom(camZoomSpeed);
            if (Gdx.input.isKeyPressed(Input.Keys.PERIOD))
                mCameraHelper.addZoom(-camZoomSpeed);
            if (Gdx.input.isKeyPressed(Input.Keys.SLASH))
                mCameraHelper.setZoom(1);
        }
    }


    private void moveCamera(float x, float y) {
        x += mCameraHelper.getPosition().x;
        y += mCameraHelper.getPosition().y;
        mCameraHelper.setPosition(x, y);
    }

    // Rectangles for collision detection
    private Rectangle r1 = new Rectangle();
    private Rectangle r2 = new Rectangle();

    private void onCollisionBunnyHeadWithRock(@NotNull Rock rock) {
        BunnyHead bunnyHead = mLevel.mBunnyHead;
        float heightDifference = Math.abs(bunnyHead.mPosition.y - (rock.mPosition.y + rock.mBounds.height));
        if (heightDifference > 0.25f) {
            boolean hitRightEdge = bunnyHead.mPosition.x > (rock.mPosition.x + rock.mBounds.width / 2.0f);
            if (hitRightEdge) {
                bunnyHead.mPosition.x = rock.mPosition.x + rock.mBounds.width;
            } else {
                bunnyHead.mPosition.x = rock.mPosition.x - bunnyHead.mBounds.width;
            }
            return;
        }
        switch (bunnyHead.mJumpState) {
            case GROUNDED:
                break;
            case FALLING:
            case JUMP_FALLING:
                bunnyHead.mPosition.y = rock.mPosition.y + bunnyHead.mBounds.height + bunnyHead.mOrigin.y;
                bunnyHead.mJumpState = BunnyHead.JUMP_STATE.GROUNDED;
                break;
            case JUMP_RISING:
                bunnyHead.mPosition.y = rock.mPosition.y + bunnyHead.mBounds.height + bunnyHead.mOrigin.y;
                break;
        }
    }

    private void onCollisionBunnyWithGoldCoin(@NotNull GoldCoin goldcoin) {
        goldcoin.mIsCollected = true;
        mScore += goldcoin.getScore();
        AudioManager.instance.play(Assets.sInstance.sounds.pickupCoin);
        Gdx.app.log(TAG, "Gold coin collected");
    }

    private void onCollisionBunnyWithFeather(@NotNull Feather feather) {
        feather.mIsCollected = true;
        mScore += feather.getScore();
        mLevel.mBunnyHead.setFeatherPowerup(true);
        AudioManager.instance.play(Assets.sInstance.sounds.pickupFeather);
        Gdx.app.log(TAG, "Feather collected");
    }

    private void onCollisionBunnyWithGoal () {
        goalReached = true;
        timeLeftGameOverDelay = Constants.TIME_DELAY_GAME_FINISHED;
        Vector2 centerPosBunnyHead =
                new Vector2(mLevel.mBunnyHead.mPosition);
        centerPosBunnyHead.x += mLevel.mBunnyHead.mBounds.width;
        spawnCarrots(centerPosBunnyHead, Constants.CARROTS_SPAWN_MAX,
                Constants.CARROTS_SPAWN_RADIUS);
    }

    private void testCollisions() {
        r1.set(mLevel.mBunnyHead.mPosition.x, mLevel.mBunnyHead.mPosition.y,
                mLevel.mBunnyHead.mBounds.width, mLevel.mBunnyHead.mBounds.height);
        // Test collision: Bunny Head <-> Rocks
        for (Rock rock : mLevel.mRocks) {
            r2.set(rock.mPosition.x, rock.mPosition.y, rock.mBounds.width, rock.mBounds.height);
            if (!r1.overlaps(r2)) continue;
            onCollisionBunnyHeadWithRock(rock);
            // IMPORTANT: must do all collisions for valid
            // edge testing on rocks.
        }

        // Test collision: Bunny Head <-> Gold Coins
        for (GoldCoin goldcoin : mLevel.mGoldCoins) {
            if (goldcoin.mIsCollected) continue;
            r2.set(goldcoin.mPosition.x, goldcoin.mPosition.y, goldcoin.mBounds.width, goldcoin.mBounds.height);
            if (!r1.overlaps(r2)) continue;
            onCollisionBunnyWithGoldCoin(goldcoin);
            break;
        }
        // Test collision: Bunny Head <-> Feathers
        for (Feather feather : mLevel.mFeathers) {
            if (feather.mIsCollected) continue;
            r2.set(feather.mPosition.x, feather.mPosition.y, feather.mBounds.width, feather.mBounds.height);
            if (!r1.overlaps(r2)) continue;
            onCollisionBunnyWithFeather(feather);
            break;
        }

        // Test collision: Bunny Head <-> Goal
        if (!goalReached) {
            r2.set(mLevel.goal.mBounds);
            r2.x += mLevel.goal.mPosition.x;
            r2.y += mLevel.goal.mPosition.y;
            if (r1.overlaps(r2)) onCollisionBunnyWithGoal();
        }
    }

    private float timeLeftGameOverDelay;
    public boolean isGameOver () {
        return mLives < 0;
    }
    public boolean isPlayerInWater () {
        return mLevel.mBunnyHead.mPosition.y < -5;
    }

    @Override
    public void dispose () {
        if (b2world != null) b2world.dispose();
    }
}
