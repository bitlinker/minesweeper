package com.bitlinker.hophop.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.bitlinker.hophop.Assets;

import org.jetbrains.annotations.NotNull;

/**
 * Created by bitlinker on 19.11.2017.
 */

public class GoldCoin extends AbstractGameObject {
    private TextureRegion mRegGoldCoin;
    public boolean mIsCollected;

    public GoldCoin() {
        init();
    }

    private void init() {
        mDimension.set(0.5f, 0.5f);
        mRegGoldCoin = Assets.sInstance.mGoldCoin.mGoldCoin;
        mBounds.set(0, 0, mDimension.x, mDimension.y);
        mIsCollected = false;
    }

    public void render(@NotNull SpriteBatch batch) {
        if (mIsCollected) return;
        TextureRegion reg;
        reg = mRegGoldCoin;
        batch.draw(reg.getTexture(),
                mPosition.x,
                mPosition.y,
                mOrigin.x,
                mOrigin.y,
                mDimension.x,
                mDimension.y,
                mScale.x,
                mScale.y,
                mRotation,
                reg.getRegionX(),
                reg.getRegionY(),
                reg.getRegionWidth(),
                reg.getRegionHeight(),
                false,
                false);
    }

    public int getScore() {
        return 100;
    }
}
