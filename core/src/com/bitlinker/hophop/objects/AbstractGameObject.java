package com.bitlinker.hophop.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import org.jetbrains.annotations.NotNull;


/**
 * Created by bitlinker on 18.11.2017.
 */

public abstract class AbstractGameObject {
    public Vector2 mPosition;
    public Vector2 mDimension;
    public Vector2 mOrigin;
    public Vector2 mScale;
    public float mRotation;

    public Vector2 mVelocity;
    public Vector2 mTerminalVelocity;
    public Vector2 mFriction;
    public Vector2 mAcceleration;
    public Rectangle mBounds;

    public Body body;

    public AbstractGameObject() {
        mPosition = new Vector2();
        mDimension = new Vector2(1, 1);
        mOrigin = new Vector2();
        mScale = new Vector2(1, 1);
        mRotation = 0;
        mVelocity = new Vector2();
        mTerminalVelocity = new Vector2(1, 1);
        mFriction = new Vector2();
        mAcceleration = new Vector2();
        mBounds = new Rectangle();
    }

    public void update(float dt) {
        if (body == null) {
            updateMotionX(dt);
            updateMotionY(dt);
            mPosition.x += mVelocity.x * dt;
            mPosition.y += mVelocity.y * dt;
        } else {
            mPosition.set(body.getPosition());
            mRotation = body.getAngle() * MathUtils.radiansToDegrees;
        }
    }

    protected void updateMotionX(float deltaTime) {
        if (mVelocity.x != 0) {
            if (mVelocity.x > 0) {
                mVelocity.x = Math.max(mVelocity.x - mFriction.x * deltaTime, 0);
            } else {
                mVelocity.x = Math.min(mVelocity.x + mFriction.x * deltaTime, 0);
            }
        }
        mVelocity.x += mAcceleration.x * deltaTime;
        mVelocity.x = MathUtils.clamp(mVelocity.x, -mTerminalVelocity.x, mTerminalVelocity.x);
    }

    protected void updateMotionY(float deltaTime) {
        if (mVelocity.y != 0) {
            if (mVelocity.y > 0) {
                mVelocity.y = Math.max(mVelocity.y - mFriction.y * deltaTime, 0);
            } else {
                mVelocity.y = Math.min(mVelocity.y + mFriction.y * deltaTime, 0);
            }
        }
        mVelocity.y += mAcceleration.y * deltaTime;
        mVelocity.y = MathUtils.clamp(mVelocity.y, - mTerminalVelocity.y, mTerminalVelocity.y);
    }

    public abstract void render(@NotNull SpriteBatch batch);
}
