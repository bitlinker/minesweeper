package com.bitlinker.hophop.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.bitlinker.hophop.Assets;

import org.jetbrains.annotations.NotNull;

/**
 * Created by bitlinker on 19.11.2017.
 */

public class Rock extends AbstractGameObject {
    private final float FLOAT_CYCLE_TIME = 2.0f;
    private final float FLOAT_AMPLITUDE = 0.25f;

    private float floatCycleTimeLeft;
    private boolean floatingDownwards;
    private Vector2 floatTargetPosition;

    private TextureRegion mRegEdge;
    private TextureRegion mRegMiddle;
    private int mLength;

    public Rock() {
        init();
    }

    private void init() {
        mDimension.set(1, 1.5f);
        mRegEdge = Assets.sInstance.mRock.mEdge;
        mRegMiddle = Assets.sInstance.mRock.mMidle;
        setLength(1);
        floatingDownwards = false;
        floatCycleTimeLeft = MathUtils.random(0, FLOAT_CYCLE_TIME / 2);
        floatTargetPosition = null;
    }

    public void setLength(int length) {
        mLength = length;
        mBounds.set(0, 0, mDimension.x * length, mDimension.y);
    }

    public void increaseLength(int amount) {
        setLength(mLength + amount);
    }

    @Override
    public void render(@NotNull SpriteBatch batch) {
        TextureRegion reg;
        float relX = 0;
        float relY = 0;

        // Draw left edge
        reg = mRegEdge;
        relX -= mDimension.x / 4;
        batch.draw(
                reg.getTexture(),
                mPosition.x + relX,
                mPosition.y + relY,
                mOrigin.x,
                mOrigin.y,
                mDimension.x / 4,
                mDimension.y,
                mScale.x,
                mScale.y,
                mRotation,
                reg.getRegionX(),
                reg.getRegionY(),
                reg.getRegionWidth(),
                reg.getRegionHeight(), false, false
        );

        // Draw middle
        relX = 0;
        reg = mRegMiddle;
        for (int i = 0; i < mLength; i++) {
            batch.draw(reg.getTexture(),
                    mPosition.x + relX,
                    mPosition.y + relY,
                    mOrigin.x,
                    mOrigin.y,
                    mDimension.x,
                    mDimension.y,
                    mScale.x,
                    mScale.y,
                    mRotation,
                    reg.getRegionX(),
                    reg.getRegionY(),
                    reg.getRegionWidth(),
                    reg.getRegionHeight(),
                    false,
                    false
            );
            relX += mDimension.x;
        }
        // Draw right edge
        reg = mRegEdge;
        batch.draw(
                reg.getTexture(),
                mPosition.x + relX,
                mPosition.y + relY,
                mOrigin.x + mDimension.x / 8,
                mOrigin.y,
                mDimension.x / 4,
                mDimension.y,
                mScale.x,
                mScale.y,
                mRotation,
                reg.getRegionX(),
                reg.getRegionY(),
                reg.getRegionWidth(),
                reg.getRegionHeight(),
                true,
                false
        );
    }

    @Override
    public void update (float deltaTime) {
        super.update(deltaTime);
        floatCycleTimeLeft -= deltaTime;
        if (floatCycleTimeLeft<= 0) {
            floatCycleTimeLeft = FLOAT_CYCLE_TIME;
            floatingDownwards = !floatingDownwards;
            body.setLinearVelocity(0, FLOAT_AMPLITUDE * (floatingDownwards ? -1 : 1));
        } else {
            body.setLinearVelocity(body.getLinearVelocity().scl(0.98f));
        }
    }
}
