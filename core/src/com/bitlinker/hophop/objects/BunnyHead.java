package com.bitlinker.hophop.objects;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.bitlinker.hophop.Assets;
import com.bitlinker.hophop.AudioManager;
import com.bitlinker.hophop.CharacterSkin;
import com.bitlinker.hophop.Constants;
import com.bitlinker.hophop.GamePreferences;

import org.jetbrains.annotations.NotNull;

/**
 * Created by bitlinker on 19.11.2017.
 */

public class BunnyHead extends AbstractGameObject {
    public static final String TAG = BunnyHead.class.getName();

    private final float JUMP_TIME_MAX = 0.3f;
    private final float JUMP_TIME_MIN = 0.1f;
    private final float JUMP_TIME_OFFSET_FLYING = JUMP_TIME_MAX - 0.018f;

    public enum VIEW_DIRECTION {LEFT, RIGHT}

    public enum JUMP_STATE {
        GROUNDED, FALLING, JUMP_RISING, JUMP_FALLING
    }

    private TextureRegion mRegHead;
    public VIEW_DIRECTION mViewDirection;
    public float mTimeJumping;
    public JUMP_STATE mJumpState;

    public boolean mHasFeatherPowerup;
    public float mTimeLeftFeatherPowerup;

    public ParticleEffect dustParticles = new ParticleEffect();

    public BunnyHead() {
        init();
    }

    public void init() {
        mDimension.set(1, 1);
        mRegHead = Assets.sInstance.mBunny.mHead;
        mOrigin.set(mDimension.x / 2, mDimension.y / 2);
        mBounds.set(0, 0, mDimension.x, mDimension.y);
        mTerminalVelocity.set(3.0f, 4.0f);
        mFriction.set(12.0f, 0.0f);
        mAcceleration.set(0.0f, -25.0f);
        mViewDirection = VIEW_DIRECTION.RIGHT;
        mJumpState = JUMP_STATE.FALLING;
        mTimeJumping = 0;

        mHasFeatherPowerup = false;
        mTimeLeftFeatherPowerup = 0;

        dustParticles.load(Gdx.files.internal("pfx/dust.pfx"), Gdx.files.internal("pfx"));
        dustParticles.start();
    }

    public void setJumping(boolean jumpKeyPressed) {
        switch (mJumpState) {
            case GROUNDED: // Character is standing on a platform
                if (jumpKeyPressed) {
                    AudioManager.instance.play(Assets.sInstance.sounds.jump);
                    mTimeJumping = 0;
                    mJumpState = JUMP_STATE.JUMP_RISING;
                }
                break;
            case JUMP_RISING: // Rising in the air
                if (!jumpKeyPressed)
                    mJumpState = JUMP_STATE.JUMP_FALLING;
                break;
            case FALLING:// Falling down
            case JUMP_FALLING: // Falling down after jump
                if (jumpKeyPressed && mHasFeatherPowerup) {
                    AudioManager.instance.play(Assets.sInstance.sounds.jumpWithFeather, 1, MathUtils.random(1.0f, 1.1f));
                    mTimeJumping = JUMP_TIME_OFFSET_FLYING;
                    mJumpState = JUMP_STATE.JUMP_RISING;
                }
                break;
        }
    }

    public void setFeatherPowerup(boolean pickedUp) {
        mHasFeatherPowerup = pickedUp;
        if (pickedUp) {
            mTimeLeftFeatherPowerup = Constants.ITEM_FEATHER_POWERUP_DURATION;
        }
    }

    public boolean hasFeatherPowerup() {
        return mHasFeatherPowerup && mTimeLeftFeatherPowerup > 0;
    }

    @Override
    public void update(float dt) {
        super.update(dt);
        if (mVelocity.x != 0) {
            mViewDirection = mVelocity.x < 0 ? VIEW_DIRECTION.LEFT : VIEW_DIRECTION.RIGHT;
        }
        if (mTimeLeftFeatherPowerup > 0) {
            mTimeLeftFeatherPowerup -= dt;
            if (mTimeLeftFeatherPowerup < 0) {
                mTimeLeftFeatherPowerup = 0;
                setFeatherPowerup(false);
            }
        }
        dustParticles.update(dt);
    }

    @Override
    protected void updateMotionY(float deltaTime) {
        switch (mJumpState) {
            case GROUNDED:
                mJumpState = JUMP_STATE.FALLING;
                break;
            case JUMP_RISING:
                mTimeJumping += deltaTime;
                if (mTimeJumping <= JUMP_TIME_MAX) {
                    mVelocity.y = mTerminalVelocity.y;
                }
                break;
            case FALLING:
                break;
            case JUMP_FALLING:
                mTimeJumping += deltaTime;
                if (mTimeJumping > 0 && mTimeJumping <= JUMP_TIME_MIN) {
                    mVelocity.y = mTerminalVelocity.y;
                }
        }

        if (mJumpState != JUMP_STATE.GROUNDED)
            //dustParticles.allowCompletion();
            super.updateMotionY(deltaTime);
    }

    @Override
    public void render(@NotNull SpriteBatch batch) {
        TextureRegion reg;

        dustParticles.draw(batch);

        batch.setColor(CharacterSkin.values()[GamePreferences.instance.charSkin].getColor());

        if (mHasFeatherPowerup) {
            batch.setColor(1.0f, 0.8f, 0.0f, 1.0f);
        }
        reg = mRegHead;
        batch.draw(reg.getTexture(), mPosition.x, mPosition.y, mOrigin.x,
                mOrigin.y, mDimension.x, mDimension.y, mScale.x, mScale.y, mRotation,
                reg.getRegionX(), reg.getRegionY(), reg.getRegionWidth(),
                reg.getRegionHeight(), mViewDirection == VIEW_DIRECTION.LEFT,
                false);
        batch.setColor(1, 1, 1, 1);
    }
}
