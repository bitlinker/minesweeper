package com.bitlinker.hophop.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.bitlinker.hophop.Assets;
import com.bitlinker.hophop.screens.AbstractGameScreen;

/**
 * Created by bitlinker on 26.11.2017.
 */

public class Carrot extends AbstractGameObject {
    private TextureRegion regCarrot;

    public Carrot() {
        init();
    }

    private void init() {
        mDimension.set(0.25f, 0.5f);
        regCarrot = Assets.sInstance.mLevelDecoration.carrot;
        mBounds.set(0, 0, mDimension.x, mDimension.y);
        mOrigin.set(mDimension.x / 2, mDimension.y / 2);
    }

    public void render(SpriteBatch batch) {
        TextureRegion reg = null;
        reg = regCarrot;
        batch.draw(reg.getTexture(), mPosition.x - mOrigin.x,
                mPosition.y - mOrigin.y, mOrigin.x, mOrigin.y, mDimension.x,
                mDimension.y, mScale.x, mScale.y, mRotation, reg.getRegionX(),
                reg.getRegionY(), reg.getRegionWidth(),
                reg.getRegionHeight(), false, false);
    }
}
