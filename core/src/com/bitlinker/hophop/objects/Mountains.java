package com.bitlinker.hophop.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.bitlinker.hophop.Assets;

import org.jetbrains.annotations.NotNull;

/**
 * Created by bitlinker on 19.11.2017.
 */

public class Mountains extends AbstractGameObject {
    private TextureRegion mRegMountainLeft;
    private TextureRegion mRegMountainRight;
    private int mLength;

    public Mountains(int length) {
        this.mLength = length;
        init();
    }

    private void init() {
        mDimension.set(10, 2);
        mRegMountainLeft = Assets.sInstance.mLevelDecoration.mMountainLeft;
        mRegMountainRight = Assets.sInstance.mLevelDecoration.mMountainRight;
        // shift mountain and extend mLength
        mOrigin.x = -mDimension.x * 2;
        mLength += mDimension.x * 2;
    }


    private void drawMountain(SpriteBatch batch,
                              float offsetX,
                              float offsetY,
                              float tintColor,
                              float parallaxSpeedX) {
        TextureRegion reg;
        batch.setColor(tintColor, tintColor, tintColor, 1);
        float xRel = mDimension.x * offsetX;
        float yRel = mDimension.y * offsetY;

        // mMountains span the whole mLevel
        int mountainLength = 0;
        mountainLength += MathUtils.ceil(mLength / (2 * mDimension.x) * (1 - parallaxSpeedX));
        mountainLength += MathUtils.ceil(0.5f + offsetX);
        for (int i = 0; i < mountainLength; i++) {
            // mountain left
            reg = mRegMountainLeft;
            batch.draw(
                    reg.getTexture(),
                    mOrigin.x + xRel + mPosition.x * parallaxSpeedX,
                    mPosition.y + mOrigin.y + yRel,
                    mOrigin.x,
                    mOrigin.y,
                    mDimension.x,
                    mDimension.y,
                    mScale.x,
                    mScale.y,
                    mRotation,
                    reg.getRegionX(),
                    reg.getRegionY(),
                    reg.getRegionWidth(),
                    reg.getRegionHeight()
                    , false,
                    false);
            xRel += mDimension.x;

            // mountain right
            reg = mRegMountainRight;
            batch.draw(
                    reg.getTexture(),
                    mOrigin.x + xRel,
                    mPosition.y + mOrigin.y + yRel,
                    mOrigin.x,
                    mOrigin.y,
                    mDimension.x,
                    mDimension.y,
                    mScale.x,
                    mScale.y,
                    mRotation,
                    reg.getRegionX(),
                    reg.getRegionY(),
                    reg.getRegionWidth(),
                    reg.getRegionHeight(),
                    false,
                    false);
            xRel += mDimension.x;
        }
        // reset color to white
        batch.setColor(1, 1, 1, 1);
    }

    @Override
    public void render(@NotNull SpriteBatch batch) {
        // distant mMountains (dark gray)
        drawMountain(batch, 0.5f, 0.5f, 0.5f, 0.8f);
        // distant mMountains (gray)
        drawMountain(batch, 0.25f, 0.25f, 0.7f, 0.5f);
        // distant mMountains (light gray)
        drawMountain(batch, 0.0f, 0.0f, 0.9f, 0.3f);
    }

    public void updateScrollPosition(Vector2 camPosition) {
        mPosition.set(camPosition.x, mPosition.y);
    }
}
