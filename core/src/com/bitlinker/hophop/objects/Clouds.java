package com.bitlinker.hophop.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;
import com.bitlinker.hophop.Assets;

import org.jetbrains.annotations.NotNull;

/**
 * Created by bitlinker on 19.11.2017.
 */

public class Clouds extends AbstractGameObject {
    private float mLength;
    private Array<TextureRegion> mRegClouds;
    private Array<Cloud> mClouds;

    private class Cloud extends AbstractGameObject {
        private TextureRegion mRegCloud;

        public Cloud() {
        }

        public void setRegion(@NotNull TextureRegion region) {
            mRegCloud = region;
        }

        @Override
        public void render(@NotNull SpriteBatch batch) {
            TextureRegion reg = mRegCloud;
            batch.draw(
                    reg.getTexture(),
                    mPosition.x + mOrigin.x,
                    mPosition.y + mOrigin.y,
                    mOrigin.x,
                    mOrigin.y,
                    mDimension.x,
                    mDimension.y,
                    mScale.x,
                    mScale.y,
                    mRotation,
                    reg.getRegionX(),
                    reg.getRegionY(),
                    reg.getRegionWidth(),
                    reg.getRegionHeight(),
                    false,
                    false);
        }
    }

    public Clouds(float length) {
        mLength = length;
        init();
    }

    private void init() {
        mDimension.set(3.0f, 1.5f);
        mRegClouds = new Array<TextureRegion>();
        mRegClouds.add(Assets.sInstance.mLevelDecoration.mCloud01);
        mRegClouds.add(Assets.sInstance.mLevelDecoration.mCloud02);
        mRegClouds.add(Assets.sInstance.mLevelDecoration.mCloud03);
        int distFac = 5;
        int numClouds = (int) (mLength / distFac);
        mClouds = new Array<Cloud>(2 * numClouds);
        for (int i = 0; i < numClouds; i++) {
            Cloud cloud = spawnCloud();
            cloud.mPosition.x = i * distFac;
            mClouds.add(cloud);
        }
    }

    private @NotNull Cloud spawnCloud() {
        Cloud cloud = new Cloud();
        cloud.mDimension.set(mDimension);
        cloud.setRegion(mRegClouds.random());

        Vector2 pos = new Vector2();
        pos.x = mLength + 10; // position after end of mLevel
        pos.y += 1.75; // base position
        pos.y += MathUtils.random(0.0f, 0.2f) * (MathUtils.randomBoolean() ? 1 : -1);
        cloud.mPosition.set(pos);
        // speed
        Vector2 speed = new Vector2();
        speed.x += 0.5f; // base speed
        speed.x += MathUtils.random(0.0f, 0.75f);
        cloud.mTerminalVelocity.set(speed);
        speed.x *= -1; // move left
        cloud.mVelocity.set(speed);
        return cloud;
    }

    @Override
    public void update(float dt) {
        for (int i = mClouds.size - 1; i >= 0; i--) {
            Cloud cloud = mClouds.get(i);
            cloud.update(dt);
            if (cloud.mPosition.x < -10) {
                mClouds.removeIndex(i);
                mClouds.add(spawnCloud());
            }
        }
    }

    @Override
    public void render(@NotNull SpriteBatch batch) {
        for (Cloud cloud : mClouds)
            cloud.render(batch);
    }
}
