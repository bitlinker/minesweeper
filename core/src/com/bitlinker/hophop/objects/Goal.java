package com.bitlinker.hophop.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.bitlinker.hophop.Assets;

/**
 * Created by bitlinker on 26.11.2017.
 */

public class Goal extends AbstractGameObject {
    private TextureRegion regGoal;

    public Goal() {
        init();
    }

    private void init() {
        mDimension.set(3.0f, 3.0f);
        regGoal = Assets.sInstance.mLevelDecoration.goal;
        mBounds.set(1, Float.MIN_VALUE, 10, Float.MAX_VALUE);
        mOrigin.set(mDimension.x / 2.0f, 0.0f);
    }

    public void render(SpriteBatch batch) {
        TextureRegion reg = null;
        reg = regGoal;
        batch.draw(reg.getTexture(), mPosition.x - mOrigin.x,
                mPosition.y - mOrigin.y, mOrigin.x, mOrigin.y, mDimension.x,
                mDimension.y, mScale.x, mScale.y, mRotation,
                reg.getRegionX(), reg.getRegionY(),
                reg.getRegionWidth(), reg.getRegionHeight(),
                false, false);
    }
}
