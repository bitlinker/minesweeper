package com.bitlinker.hophop.objects;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.bitlinker.hophop.Assets;

import org.jetbrains.annotations.NotNull;

/**
 * Created by bitlinker on 19.11.2017.
 */

public class WaterOverlay extends AbstractGameObject {
    private TextureRegion mRegWaterOverlay;
    private float mLength;

    public WaterOverlay(float length) {
        this.mLength = length;
        init();
    }

    private void init() {
        mDimension.set(mLength * 10, 3);
        mRegWaterOverlay = Assets.sInstance.mLevelDecoration.mWaterOverlay;
        mOrigin.x = -mDimension.x / 2;
    }

    @Override
    public void render(@NotNull SpriteBatch batch) {
        TextureRegion reg;
        reg = mRegWaterOverlay;
        batch.draw(
                reg.getTexture(),
                mPosition.x + mOrigin.x,
                mPosition.y + mOrigin.y,
                mOrigin.x,
                mOrigin.y,
                mDimension.x,
                mDimension.y,
                mScale.x,
                mScale.y,
                mRotation,
                reg.getRegionX(),
                reg.getRegionY(),
                reg.getRegionWidth(),
                reg.getRegionHeight(),
                false,
                false);
    }
}
